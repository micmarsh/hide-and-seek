﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XLibrary;
using XLibrary.TileEngine;
using XLibrary.SpriteClasses;

namespace XLibrary.GameScreens
{
    public class HidePlaceManager
    {
        #region Field Region
        //---Makha's code---//
        HidePlace[] hp; // an array fo the hideplace objects
        int[,] hidePlaceMap;// keeps the location of each hideplace on the map greed
        int[,] occupiedPxls;// the pixels on the map which are alredy taken by the objects
        int hidePlaceNum; // number of hiding places on the map
        static int hidePlaceSize = 256; // the size of one side of the hideplace image is 256
        Texture2D[] hidePlaceTextures; //right now only 6 different textures(tree,bush,slide,horse, elephant,sandbox)
        int hpIndex; //shows which hidePlace object the character is behind
        bool isBehind = false;//shows if the character is behind an object
        int selectedObjIndex;//index of hte hidePlace object which was selected
        //---End of Makha' code---//

        HiderSeeker hiderSeeker;//Mike's


        //Engine engine = new Engine(engineTileSize, engineTileSize);//edited from the original

        #endregion

        #region Property Region
        public HidePlace[] HidePlaceArray
        {
            get { return hp; }
        }
        #endregion

        #region Constructor Region

        public HidePlaceManager(int hpNum, int engineTileSize, int engineTileNum, Texture2D[] hpTextures)
        {
            hpIndex = -1;
            hidePlaceNum = hpNum;
            hidePlaceTextures = hpTextures;
            //Console.WriteLine(" map size is" + engineTileNum * engineTileSize+"\n");
            //Console.WriteLine(" hidePlaceSize is" + hidePlaceSize+"\n");
            //Console.WriteLine(" hidePlaceMap size is" + (engineTileNum * engineTileSize) / hidePlaceSize+"\n");
            hidePlaceMap = new int[(engineTileNum * engineTileSize) / hidePlaceSize + 2, (engineTileNum * engineTileSize) / hidePlaceSize + 2];
            for (int i = 0; i <= hidePlaceMap.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= hidePlaceMap.GetUpperBound(1); j++)
                {
                    hidePlaceMap[i, j] = -1;
                }
            }
            //new code
            occupiedPxls = new int[engineTileSize * engineTileNum, engineTileSize * engineTileNum];
            for (int i = 0; i <= occupiedPxls.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= occupiedPxls.GetUpperBound(1); j++)
                {
                    occupiedPxls[i, j] = -1;
                }
            }
            //---//
            hp = new HidePlace[hidePlaceNum];
            for (int i = 0; i < hp.Length; i++)
            {
                //Vector2 pos = findLoc(hidePlaceMap);
                Vector2 pos = new Vector2(0, 0);
                //hidePlaceMap[(int)pos.X, (int)pos.Y] = i;
                //pos.X = pos.X * hidePlaceSize;
                //pos.Y = pos.Y * hidePlaceSize;
                if (i % 7 == 0)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[0], pos, hidePlaceTextures[0].Height, hidePlaceTextures[0].Width, 1);
                }
                else if (i % 7 == 1)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[1], pos, hidePlaceTextures[1].Height, hidePlaceTextures[1].Width, 2);
                }
                else if (i % 7 == 2)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[2], pos, hidePlaceTextures[2].Height, hidePlaceTextures[2].Width, 3);
                }
                else if (i % 7 == 3)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[3], pos, hidePlaceTextures[3].Height, hidePlaceTextures[3].Width, 4);
                }
                else if (i % 7 == 4)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[4], pos, hidePlaceTextures[4].Height, hidePlaceTextures[4].Width, 5);
                }
                else if (i % 7 == 5)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[6], pos, hidePlaceTextures[6].Height, hidePlaceTextures[6].Width, 7);
                }
                else if (i % 7 == 6)
                {
                    hp[i] = new HidePlace(hidePlaceTextures[5], pos, hidePlaceTextures[5].Height, hidePlaceTextures[5].Width, 6);
                }
                hp[i].Position = findLoc2(occupiedPxls, hp[i].ActiveRec.Width, hp[i].ActiveRec.Height);

            }
            //---//
            hiderSeeker = new HiderSeeker(hp, engineTileNum * engineTileSize, engineTileNum * engineTileSize);//added by Mike

        }

        #endregion

        #region XNA Method Region



        public void Update(GameTime gameTime)
        {
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera)
        {
            for (int i = 0; i < hp.Length; i++)
            {
                if (i == selectedObjIndex) hp[i].Draw(gameTime, spriteBatch, camera, true);
                //hp[i].Draw(gameTime, spriteBatch, camera, false);
                else hp[i].Draw(gameTime, spriteBatch, camera, false);
            }
        }
        public Vector2 findLoc(int[,] hpMap)//find a location of the map if the map is represented as a grid of images
        {
            Vector2 p;
            while (true)
            {
                Random rnd = new Random();
                int posX = rnd.Next(hpMap.GetUpperBound(0));
                int posY = rnd.Next(hpMap.GetUpperBound(1));
                if (hpMap[posX, posY] == -1 && !(posX == 0 && posY == 0))
                {
                    p = new Vector2(posX, posY);
                    break;
                }
            }
            return p;
        }
        public Vector2 findLoc2(int[,] occupiedPxls, int width, int height)
        {// find location in the map represented as a pixels. 
            Vector2 p;
            bool occupied = false;
            while (true)
            {
                Random rnd = new Random();
                int posX = rnd.Next(occupiedPxls.GetUpperBound(0) - width);// subtract widths so does not go our of the boundaries
                int posY = rnd.Next(occupiedPxls.GetUpperBound(1) - height);// subtract height so does not go our of the boundaries
                for (int i = 0; i <= height; i++)
                {
                    for (int j = 0; j <= width; j++)
                    {
                        if (occupiedPxls[posX + j, posY + i] != -1)
                        {
                            occupied = true;
                            break;
                        }
                        if (occupied)
                        {
                            break;
                        }
                    }
                }
                if (!occupied)
                {
                    p = new Vector2(posX, posY);
                    for (int i = 0; i <= height; i++)
                    {
                        for (int j = 0; j <= width; j++)
                        {
                            occupiedPxls[posX + j, posY + i] = 1;
                        }
                    }
                    break;
                }
                else occupied = false;
            }
            return p;
        }
        public void objectSelected(Vector2 mousePos)
        {
            Point p = new Point(Convert.ToInt32(mousePos.X), Convert.ToInt32(mousePos.Y));
            bool isSelected = false;
            Rectangle tempActiveRec;
            Rectangle tempBaseRec;
            for (int i = 0; i < hp.Length; i++)
            {
                tempActiveRec = hp[i].ActiveRec;
                tempActiveRec.X = tempActiveRec.X + (int)hp[i].Position.X;
                tempActiveRec.Y = tempActiveRec.Y + (int)hp[i].Position.Y;
                tempBaseRec = hp[i].ActiveRec;
                tempBaseRec.X = tempBaseRec.X + (int)hp[i].Position.X;
                tempBaseRec.Y = tempBaseRec.Y + (int)hp[i].Position.Y;
                if (tempActiveRec.Contains(p) || tempBaseRec.Contains(p))
                {
                    //Console.WriteLine("mouse position is: " + mousePos.X + "," + mousePos.Y);
                    //Console.WriteLine("point p position is: "+p.X+","+p.Y);
                    //Console.WriteLine("active rec position is: " + tempActiveRec.X + "," + tempActiveRec.Y);
                    selectedObjIndex = i;
                    isSelected = true;
                }
            }
            if (!isSelected) selectedObjIndex = -1;
        }
        public int checkCollision(Vector2 pos, Rectangle rec)
        {
            return checkCollision(pos, rec, true);
            //TODO: edit thing below to work with baserec, or change it back to how it was
        }
        public int checkCollision(Vector2 pos, Rectangle rec, bool activerec)
        {
            Rectangle rect;

            Rectangle playerRect = new Rectangle((int)(pos.X + rec.X), (int)(pos.Y + rec.Y), rec.Width, rec.Height);

            isBehind = false;
            for (int i = 0; i < hp.Length; i++)
            {

                if (activerec)
                    rect = hp[i].ActiveRec;
                else
                    rect = hp[i].BaseRec;

                rect.X += (int)hp[i].Position.X;
                rect.Y += (int)hp[i].Position.Y;


                //Console.WriteLine(rect.X+" object rectangle "+rect.Y);

                //Console.WriteLine(rec.X + " character rectangle " + rec.Y);

                if (rect.Intersects(playerRect))
                {
                    hpIndex = i;
                    return i;
                }
                else hpIndex = -1;
            }
            return -1;
        }


        //Mike's function
        public FacialExpression checkHidden(int index)
        {


            if (index > -1)
            {
                return hiderSeeker.check(hp[index]);
            }
            else
                return FacialExpression.neutral;
        }


        #endregion

        #region statistics region

        public int CurrentCheckCount
        {
            get { return hiderSeeker.CurrentCheckCount; }
        }

        public int[] AllCheckCounts
        {
            get { return hiderSeeker.AllCheckCounts; }
        }

        #endregion
    }
}
