﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XLibrary.Controls
{
    public class Button : Control
    {
        #region Fields and Properties

        Color selectedColor = Color.Blue;
        Texture2D buttonImage;

        public Color SelectedColor
        {
            get { return selectedColor; }
            set { selectedColor = value; }
        }

        public Texture2D ButtonImage
        {
            get { return buttonImage; }
            set { buttonImage = value; }
        }

        #endregion

        #region Constructor Region

        public Button(Texture2D image)
        {
            TabStop = true;
            HasFocus = false;
            Position = Vector2.Zero;
            Type = "Button";
            ButtonImage = image;
            Size = new Vector2(image.Width, image.Height);
        }

        #endregion

        #region Abstract Methods

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (hasFocus)
                spriteBatch.Draw(buttonImage, position, new Color(new Vector4(1f, 1f, 1f, 0.5f)));
            else
                spriteBatch.Draw(buttonImage, position, Color.White);
        }

        public override void HandleInput(PlayerIndex playerIndex)
        {
            if (!HasFocus)
                return;

            if (InputHandler.KeyReleased(Keys.Enter) ||
                InputHandler.threeSecondCheck() ||
                (InputHandler.mouseReleased() && isMouseOver(InputHandler.lastMouseReleasePoint())) ||
                InputHandler.ButtonReleased(Buttons.A, playerIndex))
                base.OnSelected(null);
        }

        #endregion
    }
}
