﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using XLibrary.TileEngine;

namespace XLibrary.SpriteClasses
{
    public class SwitchTextManager
    {
        
        #region field region
        Texture2D initialImage;
        Texture2D foundImage;
        Texture2D notFoundImage;
        SwitchText switchText;
        int lastLoc = -1;

        #endregion

        #region constructor region
        public SwitchTextManager(Texture2D initialImage,Texture2D foundImage,Texture2D notFoundImage)
        {
            this.initialImage = initialImage;
            this.foundImage = foundImage;
            this.notFoundImage = notFoundImage;
        }
        #endregion

        //call this based on results of check 
        public void makeText(Vector2 position,Boolean found,int hpIndex)
        {

            if (hpIndex != lastLoc)
            {
                Texture2D texture;
                if (found)
                    texture = foundImage;
                else
                    texture = notFoundImage;
                Dictionary<AnimationKey, Animation> toPass = new Dictionary<AnimationKey, Animation>();
                toPass.Add(AnimationKey.Down, new Animation(2, 200, 60, 0, 0));

                switchText = new SwitchText(initialImage, texture, toPass);
                switchText.Position = position;
                hpIndex = lastLoc;
            }
            
        }

        public void Draw(SpriteBatch spriteBatch, Camera camera)
        {
            if (null != switchText)
            {
                switchText.Draw(spriteBatch, camera);
                
            }
        }

        public void Update(GameTime gameTime)
        {
            if (null != switchText)
            {
                switchText.Update(gameTime) ;

            }
        }
    }
}
