﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XLibrary.TileEngine;

namespace XLibrary.SpriteClasses
{
    public class AnimatedSprite
    {
        #region Field Region

        protected Dictionary<AnimationKey, Animation> animations;
        AnimationKey currentAnimation;
        bool isAnimating;

       protected Texture2D texture;
        Vector2 position;
        Vector2 centerPosition;
        Vector2 velocity;
        float speed = 2.0f;

        #endregion

        #region Property Region

        public AnimationKey CurrentAnimation
        {
            get { return currentAnimation; }
            set { currentAnimation = value; }
        }

        public Animation CurrentImage
        {
            get { return animations[currentAnimation]; }
      
        }

        public bool IsAnimating
        {
            get { return isAnimating; }
            set { isAnimating = value; }
        }

        public int Width
        {
            get { return animations[currentAnimation].FrameWidth; }
        }

        public int Height
        {
            get { return animations[currentAnimation].FrameHeight; }
        }

        public float Speed
        {
            get { return speed; }
            set { speed = MathHelper.Clamp(speed, 1.0f, 16.0f); }
        }

        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
            }
        }

        public Vector2 CenterPosition
        {
            get { return new Vector2(position.X + animations[currentAnimation].FrameWidth / 2,
                                     position.Y + animations[currentAnimation].FrameHeight * 3 / 4); }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
            set
            {
                velocity = value;
                if (velocity != Vector2.Zero)
                    velocity.Normalize();
            }
        }

        #endregion

        #region Constructor Region

        public AnimatedSprite(Texture2D sprite, Dictionary<AnimationKey, Animation> animation)
        {
            texture = sprite;
            animations = new Dictionary<AnimationKey, Animation>();

            foreach (AnimationKey key in animation.Keys)
                animations.Add(key, (Animation)animation[key].Clone());

        }

        #endregion

        #region Method Region

        public void Update(GameTime gameTime)
        {
            if (isAnimating)
                animations[currentAnimation].Update(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera)


        {
            
            spriteBatch.Draw(
                texture,
                position - camera.Position,
               animations[currentAnimation].CurrentFrameRect,
                Color.White);
        }

        //Mike's: draws things that aren't players
        public void Draw(SpriteBatch spriteBatch,Camera camera)
        {
            spriteBatch.Draw(
                texture,
                position - camera.Position,
                 animations[currentAnimation].CurrentFrameRect,
                 Color.White);
        }

        public void LockToMap()
        {
            position.X = MathHelper.Clamp(position.X, 0, TileMap.WidthInPixels - Width);
            position.Y = MathHelper.Clamp(position.Y, 0, TileMap.HeightInPixels - Height);
        }

        #endregion
    }
}
