﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using XLibrary.TileEngine;


using Microsoft.Xna.Framework;


//Everything here: Mike's code
namespace XLibrary
{
    public enum FacialExpression { saddest, sadder, sad, happy, happier, happiest, found, neutral };
    
    class HiderSeeker
    {
        #region variable region
        Random rand = new Random();
        Dictionary<HidePlace, int> distances;
        Point hiddenAt;
        int mapHeight, mapWidth;
         HidePlace[] hideplaces;
        int[] distRanges = new int[5];

        Vector2 lastPosition;
        static List<int> checks = new List<int>();
        static List<int> walkedDistances = new List<int>();

     //   private int difficulty;
        

        #endregion

    #region literal region
        const double FractionOfMap = 0.125; 

    #endregion

        #region constructor region
        public HiderSeeker(HidePlace[] places, int height, int width)
        {

            //StreamReader infile = new StreamReader(@"checks.txt");
          //  List<string> in = new List<string>();
            StreamWriter outfile = new StreamWriter(@"checks.txt");
            if (checks.Count != 0)
            {

                foreach (int checkNum in checks)
                {
                    outfile.Write(checkNum.ToString());
                }

            }

            outfile.Close();

            hideplaces = places;

            
            mapHeight = height;
            mapWidth = width;

            hide();

        }
        #endregion


        #region public methods region


     /*_   public int Difficulty
        {
            set { difficulty = value; }
        }*/

        public void hide()
        {

            distances =  new Dictionary<HidePlace, int>();
            int place = rand.Next(hideplaces.Length);


            distances.Add(hideplaces[place], 0);

            hiddenAt = new Point((int)hideplaces[place].Position.X, (int)hideplaces[place].Position.Y);


            foreach (HidePlace hp in hideplaces)
            {
                if (!hideplaces[place].Equals(hp))
                {
                    Point toComp = new Point((int)hp.Position.X, (int)hp.Position.Y);
                    int distance = Distance(toComp, hiddenAt);
                    distances.Add(hp, distance);
                }


            }

           int mapPieces = (int)(FractionOfMap * Distance(new Point(0,0),new Point(mapHeight,mapWidth)));
            int curDistance = 0;
            for(int i = 4; i >= 0; i--){
                curDistance += mapPieces;
                distRanges[i] = curDistance;
               //   Console.WriteLine("Distance, fuck yeah: " + distRanges[i]);
            }

            checks.Add(0);
            walkedDistances.Add(0);
            
        }

        public FacialExpression check(HidePlace hp)
        {
            int distance = distances[hp];

            checks[checks.Count - 1]++;

            if (lastPosition != null)
            {
                Point p0 = new Point((int)lastPosition.X, (int)lastPosition.Y);
                Point p1 = new Point((int)hp.Position.X, (int)hp.Position.Y);
                walkedDistances[walkedDistances.Count - 1] += Distance(p0, p1);

                lastPosition = hp.Position;
            }


            if (distance == 0)
                return FacialExpression.found;

            int toRet = -1;

            for (int i = 0; i < 5; i++)
            {

                if (distance > distRanges[i])
                {
                    toRet = i;
                    if (Options.Difficulty == Difficulty.MEDIUM)
                    {
                        if (toRet == 0) toRet = 1;
                        else if (toRet == 5) toRet = 4;
                    }
                    else if (Options.Difficulty == Difficulty.HARD)
                    {
                        if (toRet == 0) toRet = 1;
                        if (toRet == 1) toRet = 2;

                        if (toRet == 5) toRet = 4;
                        if (toRet == 4) toRet = 3;
                    }
                    return (FacialExpression)i;
                }
            }


        
            
            return FacialExpression.happiest;
           
        }


        #endregion

        #region statistics region

        public int CurrentCheckCount{
            get { return checks[checks.Count - 1]; }
        }
        

        public int[] AllCheckCounts
        {
           get{
            int[] all = new int[checks.Count];
            int index = 0;
            foreach (int integer in checks)
            {
                all[index] = integer;
                index++;
            }


            return all;
           }
        }

        public int CurrentWalkedDistance
        {
            get { return walkedDistances[walkedDistances.Count - 1]; }
        }

        public int[] AllWalkedDistances
        {
            get{
                int[] all = new int[walkedDistances.Count];
                int index = 0;
                foreach (int integer in walkedDistances)
                {
                    all[index] = integer;
                    index++;
                }


                return all;
            }

        }

        #endregion

        #region private utilities region
        private int Distance(Point p1, Point p2)
        {

        //    Console.WriteLine("Point fuckin' one: " + p1.ToString()+" Point fuckin' two: "+p2.ToString());

            int x1 = p1.X;

            int x2 = p2.X;

            int y1 = p1.Y;
            int y2 = p2.Y;

            int result = 0;
            //Take x2-x1, then square it
            double part1 = Math.Pow((x2 - x1), 2);
            //Take y2-y1, then sqaure it
            double part2 = Math.Pow((y2 - y1), 2);

       //     Console.WriteLine("part fuckin one: " + part1 + " part fuckin' two: " + part2);
            //Add both of the parts together
            double underRadical = part1 + part2;
            //Get the square root of the parts
            result = (int)Math.Sqrt(underRadical);
     //       Console.WriteLine("What the fuck, result?: " + result);
            //Return our result
            return result;
        }
        #endregion
    }
}
