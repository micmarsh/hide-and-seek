﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XLibrary
{
    public enum Difficulty { EASY, MEDIUM, HARD };
    public class Options
    {

        private static Difficulty difficulty = Difficulty.HARD;
        public static Difficulty Difficulty
        {
            get { return difficulty; }
            set { difficulty = value; }
        }
    }
}
