﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using XLibrary;
using XLibrary.Controls;

namespace XLibrary.GameScreens
{
    public class StartMenuScreen : GameState
    {
        #region Field region

        PictureBox backgroundImage;
        PictureBox arrowImage;
        Button playButton;
        Button optionsButton;
        Button statsButton;
        Button achButton;
        Button exitButton;
        Button controlsButton;
        float maxItemWidth = 0f;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public StartMenuScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
        }

        #endregion

        #region XNA Method Region

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            ContentManager Content = Game.Content;

            backgroundImage = new PictureBox(
                Content.Load<Texture2D>(@"Backgrounds\playground1"), 
                GameRef.ScreenRectangle);
            ControlManager.Add(backgroundImage);

            Texture2D arrowTexture = Content.Load<Texture2D>(@"GUI\leftarrowUp");

            //startGame = new LinkLabel();
            //startGame.Text = "Start Game";
            //startGame.Size = startGame.SpriteFont.MeasureString(startGame.Text);
            //startGame.Selected +=new EventHandler(menuItem_Selected);

            //ControlManager.Add(startGame);

            //openOptions = new LinkLabel();
            //openOptions.Text = "Options";
            //openOptions.Size = openOptions.SpriteFont.MeasureString(openOptions.Text);
            //openOptions.Selected += menuItem_Selected;

            //ControlManager.Add(openOptions);

            //openStats = new LinkLabel();
            //openStats.Text = "Statistics";
            //openStats.Size = openStats.SpriteFont.MeasureString(openStats.Text);
            //openStats.Selected += menuItem_Selected;
            //ControlManager.Add(openStats);

            //openAchievements = new LinkLabel();
            //openAchievements.Text = "Achievements";
            //openAchievements.Size = openAchievements.SpriteFont.MeasureString(openAchievements.Text);
            //openAchievements.Selected += menuItem_Selected;
            //ControlManager.Add(openAchievements);

            //exitGame = new LinkLabel();
            //exitGame.Text = "Exit";
            //exitGame.Size = exitGame.SpriteFont.MeasureString(exitGame.Text);
            //exitGame.Selected += menuItem_Selected;

            //ControlManager.Add(exitGame);

            Texture2D playTexture = Content.Load<Texture2D>(@"Buttons\play_button_resized");
            // playTexture.
            playButton = new Button(playTexture);
            playButton.Position = new Vector2(100, 20);
            playButton.Selected += new EventHandler(menuItem_Selected);

            ControlManager.Add(playButton);

            Texture2D optsTexture = Content.Load<Texture2D>(@"Buttons\settings_button_resized");
            optionsButton = new Button(optsTexture);
            optionsButton.Position = new Vector2(600, 20);
            optionsButton.Selected += new EventHandler(menuItem_Selected);

            ControlManager.Add(optionsButton);

            Texture2D statsTexture = Content.Load<Texture2D>(@"Buttons\statistics_button_resized");
            statsButton = new Button(statsTexture);
            statsButton.Position = new Vector2(100, 270);
            statsButton.Selected += new EventHandler(menuItem_Selected);

            ControlManager.Add(statsButton);

            Texture2D achTexture = Content.Load<Texture2D>(@"Buttons\achievements_button_resized");
            achButton = new Button(achTexture);
            achButton.Position = new Vector2(600, 300);
            achButton.Selected += new EventHandler(menuItem_Selected);

            ControlManager.Add(achButton);

            Texture2D exitTexture = Content.Load<Texture2D>(@"Buttons\exit_button_resized");
            exitButton = new Button(exitTexture);
            exitButton.Position = new Vector2(600, 550);
            exitButton.Selected += new EventHandler(menuItem_Selected);
            ControlManager.Add(exitButton);

            Texture2D controlsTexture = Content.Load<Texture2D>(@"Buttons\controls_button_resized");
            controlsButton = new Button(controlsTexture);
            controlsButton.Position = new Vector2(100, 480);
            controlsButton.Selected += new EventHandler(menuItem_Selected);
            ControlManager.Add(controlsButton);

            ControlManager.NextControl();

            //ControlManager.FocusChanged += new EventHandler(ControlManager_FocusChanged);

            //ControlManager_FocusChanged(startGame, null);
        }

        //void ControlManager_FocusChanged(object sender, EventArgs e) //unneeded event handler
        //{
        //    Control control = sender as Control;
        //    Vector2 position = new Vector2(control.Position.X + maxItemWidth + 10f, control.Position.Y);
        //    arrowImage.SetPosition(position);
        //}

        private void menuItem_Selected(object sender, EventArgs e)
        {
            if (sender == playButton)
            {
                GamePlayScreen gs = GameRef.newGamePlayScreen();
                StateManager.PushState(gs);
            }

            if (sender == optionsButton)
            {
                StateManager.PushState(GameRef.OptionsScreen);
            }

            if (sender == achButton)
            {
                StateManager.PushState(GameRef.AchievementsScreen);
            }

            if (sender == statsButton)
            {
                StateManager.PushState(GameRef.StatsScreen);
            }

            if (sender == exitButton)
            {
                GameRef.Exit();
            }
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime, PlayerIndex.One);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin();

            ControlManager.Draw(GameRef.SpriteBatch);

            base.Draw(gameTime);

            GameRef.SpriteBatch.End();
        }

        #endregion

        #region Game State Method Region
        #endregion

    }
}
