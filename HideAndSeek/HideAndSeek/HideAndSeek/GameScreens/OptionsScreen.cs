﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using XLibrary;
using XLibrary.Controls;

namespace XLibrary.GameScreens
{
    public class OptionsScreen : GameState
    {
        #region Field region

        PictureBox backgroundImage;
        Label difficultyLbl;
        LinkLabel difficultySelector;
        LinkLabel exitOptions;
        float maxItemWidth = 0f;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public OptionsScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
        }

        #endregion

        #region XNA Method Region

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            ContentManager Content = Game.Content;

            backgroundImage = new PictureBox(
                Content.Load<Texture2D>(@"Backgrounds\playground1"),
                GameRef.ScreenRectangle);
            ControlManager.Add(backgroundImage);

            Texture2D arrowTexture = Content.Load<Texture2D>(@"GUI\leftarrowUp");

            difficultyLbl = new Label();
            difficultyLbl.Text = "Easy";
            difficultyLbl.Size = difficultyLbl.SpriteFont.MeasureString("Medium");
            difficultyLbl.Selected += new EventHandler(menuItem_Selected);
            difficultyLbl.Position = new Vector2(580, 100);

            ControlManager.Add(difficultyLbl);

            difficultySelector = new LinkLabel();
            difficultySelector.Text = "Difficulty:";
            difficultySelector.Size = difficultySelector.SpriteFont.MeasureString(difficultySelector.Text);
            difficultySelector.Selected += new EventHandler(menuItem_Selected);

            ControlManager.Add(difficultySelector);

            exitOptions = new LinkLabel();
            exitOptions.Text = "Back";
            exitOptions.Size = exitOptions.SpriteFont.MeasureString(exitOptions.Text);
            exitOptions.Selected += menuItem_Selected;

            ControlManager.Add(exitOptions);

            ControlManager.NextControl();

            ControlManager.FocusChanged += new EventHandler(ControlManager_FocusChanged);

            Vector2 position = new Vector2(150, 100);
            foreach (Control c in ControlManager)
            {
                if (c is LinkLabel)
                {
                    if (c.Size.X > maxItemWidth)
                        maxItemWidth = c.Size.X;

                    c.Position = position;
                    position.Y += c.Size.Y + 5f;
                }
            }

            ControlManager_FocusChanged(difficultySelector, null);
        }

        void ControlManager_FocusChanged(object sender, EventArgs e)
        {
            Control control = sender as Control;
            Vector2 position = new Vector2(control.Position.X + maxItemWidth + 10f, control.Position.Y);
        }

        private void menuItem_Selected(object sender, EventArgs e)
        {
            if (sender == difficultySelector)
            {
                changeDifficulty();
            }

            if (sender == exitOptions)
            {
                StateManager.PopState(); // pops off options state
            }
        }

        void changeDifficulty()
        {
            if (difficultyLbl.Text == "Easy")
            {
                difficultyLbl.Text = "Medium";
            }
            else if (difficultyLbl.Text == "Medium")
            {
                difficultyLbl.Text = "Hard";
            }
            else if (difficultyLbl.Text == "Hard")
            {
                difficultyLbl.Text = "Easy";
            }
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime, PlayerIndex.One);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin();

            ControlManager.Draw(GameRef.SpriteBatch);

            base.Draw(gameTime);

            GameRef.SpriteBatch.End();
        }

        #endregion

        #region Game State Method Region
        #endregion

    }
}
