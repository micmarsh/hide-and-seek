﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XLibrary;
using XLibrary.TileEngine;
using XLibrary.SpriteClasses;


namespace XLibrary.GameScreens
{
    public class GamePlayScreen : GameState
    {
        #region Field Region

        //---Makha's code---//
        Texture2D[] hidePlaceTextures; //right now only 6 different textures(tree,bush,slide,horse, elephant,sandbox)
        static int hidePlaceNum = 5; // number of hiding places on the map
        static HidePlaceManager hpManager;
        static int engineTileNum = 50;//The whole map consists of (engineTileNum*engineTileNum) tiles
        static int engineTileSize = 32;// the size of each tile. The size of the map in pixels is
        //(engineTileNum*engineTileSize)*(engineTileNum*engineTileSize)
        bool isBehind = false;//shows if the character is behind an object
        int hpIndex; //shows which hidePlace object the character is behind

        //Tileset faces;
        FaceManager faceMan;
        Texture2D[,] faces;
        int faceNum;
        int faceSetNum;
        int currentFace;
        //Fonts
        SpriteFont BritannicBold;


        int lastChecked = -1;//Mike's

        Texture2D spriteSheet;
        //---End of Makha' code---//

        //       Vector2 indicatorPos;//Mike's



        Song gamePlayTheme;//Mike's
        int winTimer = 0;//Mike's


        TileMap map;
        Camera camera;
        AnimatedSprite player;
        int Level;

        bool movingToMouse = false;
        Vector2 newLoc = Vector2.Zero;
        Vector2 lastDistance = Vector2.Zero;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region


        public GamePlayScreen(Game game, GameStateManager manager, int level)
            : base(game, manager)
        {

            camera = new Camera(GameRef.ScreenRectangle);
            currentFace = 0;//Makha's code
            hidePlaceTextures = new Texture2D[7];

            Level = level;
            hidePlaceNum = hidePlaceNum + level * 2;
            if (hidePlaceNum > 20)
            {
                hidePlaceNum = 20;
            }


            //faces = new Tileset();
        }



        #endregion

        #region XNA Method Region

        public override void Initialize()
        {
            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            //---Makha's code---//
            //initiaizing HidePlace object
            hidePlaceTextures[0] = Game.Content.Load<Texture2D>(@"HidePlaces\Hideplace1_tree_256x256");
            hidePlaceTextures[1] = Game.Content.Load<Texture2D>(@"HidePlaces\Hideplace2_bush_256x256");
            hidePlaceTextures[2] = Game.Content.Load<Texture2D>(@"HidePlaces\slider1_384x384 - colored_background");
            hidePlaceTextures[3] = Game.Content.Load<Texture2D>(@"HidePlaces\Hideplace3_horse2_256x256");
            hidePlaceTextures[4] = Game.Content.Load<Texture2D>(@"HidePlaces\elephant1_256x256_background");
            hidePlaceTextures[5] = Game.Content.Load<Texture2D>(@"HidePlaces\sand_box1_colored_deleted_resized");
            hidePlaceTextures[6] = Game.Content.Load<Texture2D>(@"HidePlaces\trash_can1_resized");
            hpManager = new HidePlaceManager(hidePlaceNum, engineTileSize, engineTileNum, hidePlaceTextures);
            BritannicBold = Game.Content.Load<SpriteFont>(@"Fonts\ClickCounterFont");
            //---End of Makha's code---//
            spriteSheet = Game.Content.Load<Texture2D>(@"PlayerSprites\boy_tileset1_768x256_colored");

            //Mike's
            gamePlayTheme = Game.Content.Load<Song>(@"Music\The Seeker");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(gamePlayTheme);
            //****end of Mike's****


            Dictionary<AnimationKey, Animation> animations = new Dictionary<AnimationKey, Animation>();

            Animation animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Down, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Left, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Right, animation);

            animation = new Animation(3, 256, 256, 0, 0);
            animations.Add(AnimationKey.Up, animation);

            player = new AnimatedSprite(spriteSheet, animations);


            base.LoadContent();

            Texture2D tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset1");
            Tileset tileset1 = new Tileset(tilesetTexture, 8, 8, 32, 32);

            tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset2");
            Tileset tileset2 = new Tileset(tilesetTexture, 8, 8, 32, 32);

            //---Makha's code---//
            faceSetNum = 1;
            faceNum = 7;
            faces = new Texture2D[faceSetNum, faceNum];
            faces[0, 0] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_crop1");
            faces[0, 1] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_happy1");
            faces[0, 2] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_happy2");
            faces[0, 3] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_happy3");
            faces[0, 4] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_sad1");
            faces[0, 5] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_sad2");
            faces[0, 6] = Game.Content.Load<Texture2D>(@"Faces\face_set1\amir_sad3");
            faceMan = new FaceManager(faces, faceNum, faceSetNum);
            //faces = new Tileset(tilesetTexture, 3, 1, 256, 256);
            //---end of Makha's code---//

            List<Tileset> tilesets = new List<Tileset>();
            tilesets.Add(tileset1);
            tilesets.Add(tileset2);
            //---Makha's code---//
            //tilesets.Add(faces);
            //---end of Makha's code---//

            MapLayer layer = new MapLayer(engineTileNum, engineTileNum);

            for (int y = 0; y < layer.Height; y++)
            {
                for (int x = 0; x < layer.Width; x++)
                {
                    Tile tile = new Tile(0, 0);

                    layer.SetTile(x, y, tile);
                }
            }

            MapLayer splatter = new MapLayer(engineTileNum, engineTileNum);

            Random random = new Random();

            for (int i = 0; i < 80; i++)
            {
                int x = random.Next(0, 40);
                int y = random.Next(0, 40);
                int index = random.Next(2, 14);

                Tile tile = new Tile(index, 0);
                splatter.SetTile(x, y, tile);
            }

            List<MapLayer> mapLayers = new List<MapLayer>();
            mapLayers.Add(layer);
            mapLayers.Add(splatter);

            map = new TileMap(tilesets, mapLayers, engineTileSize, engineTileSize);


        }

        public override void Update(GameTime gameTime)
        {
            camera.Update(gameTime);
            player.Update(gameTime);

            InputHandler.LiftOffGesture();
            if (InputHandler.KeyReleased(Keys.P))
            {
                StateManager.PushState(GameRef.PauseScreen);
            }

        
            Vector2 motion = new Vector2();
            Vector2 lastPosition = player.Position;
            Vector2 currLoc = new Vector2(player.Position.X, player.Position.Y);

            if (InputHandler.threeSecondCheck() || InputHandler.mouseReleased()) // mouse click detected
            {
                Vector2 clickLoc;

                clickLoc = (InputHandler.mouseReleased()) ? InputHandler.lastMouseReleasePoint() :
                                        new Vector2(InputHandler.hpos.X, InputHandler.hpos.Y);

                if (clickLoc.X >= 0 && clickLoc.X <= camera.ViewportRectangle.Width &&
                        clickLoc.Y >= 0 && clickLoc.Y <= camera.ViewportRectangle.Height && clickLoc != Vector2.Zero) // check if within bounds
                {
                    clickLoc = clickLoc - (player.CenterPosition - player.Position);
                    newLoc = new Vector2(MathHelper.Clamp(clickLoc.X + camera.Position.X, 0, TileMap.WidthInPixels),
                                         MathHelper.Clamp(clickLoc.Y + camera.Position.Y, 0, TileMap.HeightInPixels));
                }
                movingToMouse = true; //set to persist across multiple updates

            }

            if (movingToMouse)
            {
                double movingAngle = MathHelper.ToDegrees((float)Math.Atan2(newLoc.Y - currLoc.Y, newLoc.X - currLoc.X));
                double toleranceAngle = 25; // please set this between 0<X<45 degrees
                if (Math.Abs(movingAngle) < toleranceAngle)
                {
                    //moving right
                }
                else if (movingAngle >= toleranceAngle && movingAngle < 90 - toleranceAngle)
                {
                    //moving down + right
                }
                else if (movingAngle >= 90 - toleranceAngle && movingAngle < 90 + toleranceAngle)
                {
                    //moving down
                }
                else if (movingAngle >= 90 + toleranceAngle && movingAngle < 180 - toleranceAngle)
                {
                    //moving down + left
                }
                else if (Math.Abs(movingAngle) >= 180 - toleranceAngle)
                {
                    //moving left
                }
                else if (movingAngle >= -180 + toleranceAngle && movingAngle < -90 - toleranceAngle)
                {
                    //moving up + left
                }
                else if (movingAngle >= -90 - toleranceAngle && movingAngle < -90 + toleranceAngle)
                {
                    //moving up
                }
                else if (movingAngle >= -90 + toleranceAngle && movingAngle < 0 - toleranceAngle)
                {
                    //moving up + right
                }

                motion = newLoc - currLoc;

                if (motion != Vector2.Zero)
                { motion.Normalize(); }

                //Console.Out.Write(motion.X + " " + motion.Y + "\n");
                Vector2 distance = new Vector2(Math.Abs(newLoc.X - player.Position.X), Math.Abs(newLoc.Y - player.Position.Y));
                if (distance.X < 2) distance.X = 0;
                if (distance.Y < 2) distance.Y = 0;
                //this is a hack-ish way to stop the character from spazzing out near the edges
                Vector2 distanceDiff = new Vector2(Math.Abs(distance.X - lastDistance.X), Math.Abs(distance.Y - lastDistance.Y));
                lastDistance = distance;
                //Console.Out.Write("distance: " + distanceDiff.X + " " + distanceDiff.Y + "\n");
                if ((distance.X == 0 && distance.Y == 0) || (distanceDiff.X < 1 && distanceDiff.Y < 1))
                //Camera has reached the final position (there should probably be a more elegant way to do this)
                {
                    movingToMouse = false;
                }
            }

            //Console.Write("currLoc " + currLoc + " newLoc " + newLoc + "\n");
            if (InputHandler.KeyDown(Keys.W) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickUp, PlayerIndex.One))
            {
                player.CurrentAnimation = AnimationKey.Up;
                motion.Y = -1;
            }
            else if (InputHandler.KeyDown(Keys.S) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickDown, PlayerIndex.One))
            {
                player.CurrentAnimation = AnimationKey.Down;
                motion.Y = 1;
            }

            if (InputHandler.KeyDown(Keys.A) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickLeft, PlayerIndex.One))
            {
                player.CurrentAnimation = AnimationKey.Left;
                motion.X = -1;
            }
            else if (InputHandler.KeyDown(Keys.D) ||
                InputHandler.ButtonDown(Buttons.LeftThumbstickRight, PlayerIndex.One))
            {
                player.CurrentAnimation = AnimationKey.Right;
                motion.X = 1;
            }

            //---Makha's code---//
            //checking if character is behind the object or not
            Rectangle spriteRec = new Rectangle(90, 170, 100, 40);
            hpIndex = hpManager.checkCollision(player.Position, spriteRec, true);
            if (hpIndex != -1)
            {
                isBehind = true;

            }


            else isBehind = false;

            //checking if hand has selected the HidePlaceObject
            Vector2 handPos = new Vector2(InputHandler.hpos.X, InputHandler.hpos.Y) + camera.Position;
            //Vector2 handPos = new Vector2(InputHandler.mousePosition().X, InputHandler.mousePosition().Y) + player.Camera.Position;
            hpManager.objectSelected(handPos);
            //---End of Makha's code---//

            //Mike's codeblock
              
            int collisionIndex = hpManager.checkCollision(player.Position, spriteRec, true);

            //Mike's
            if (winTimer > 0)
            {
                winTimer++;
                if (winTimer > 50)
                {
                    MediaPlayer.Stop();
                    StateManager.PushState(GameRef.FoundIt);
                }
                  

            }


            if(lastChecked != collisionIndex)
            if ((InputHandler.LiftOffGesture() && collisionIndex != -1) || InputHandler.KeyReleased(Keys.Space))
            {
                /*  motion.X = -1 * motion.X;
                  motion.Y = -1 * motion.Y;
                  movingToMouse = false;*/

                lastChecked = collisionIndex;

                int faceRange = (int)hpManager.checkHidden(collisionIndex);

                if (faceRange == 0)
                    currentFace = 6;
                if (faceRange == 1)
                    currentFace = 5;
                if (faceRange == 2)
                    currentFace = 4;
                if (faceRange == 3)
                    currentFace = 1;
                if (faceRange == 4)
                    currentFace = 2;
                if (faceRange == 5)
                    currentFace = 3;
                if (faceRange == 7)
                    currentFace = 0;

                if (faceRange == 6)
                {
                    currentFace = 3;
                    winTimer++;
                }

             
                
                Vector2 newPosition = hpManager.HidePlaceArray[collisionIndex].Position - camera.Position;
                faceMan.setFloatParamiters(newPosition, 100, 100);
                faceMan.IsFloating = true;
                faceMan.Velocity = new Vector2(5, 5);
                faceMan.setCurrentFace(0, currentFace);

            }//**end Mike's codeblock***

            if (motion != Vector2.Zero)
            {
                player.IsAnimating = true;

        

                player.Position += motion * player.Speed;
                player.LockToMap();

                camera.LockToSprite(player);
            }
            else
            {
                player.IsAnimating = false;
            }
            faceMan.Update(gameTime);//Makha
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin(
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                Matrix.Identity);

            map.Draw(GameRef.SpriteBatch, camera);
            hpManager.Draw(gameTime, GameRef.SpriteBatch, camera);//hidePlace is being drawn
          
            player.Draw(gameTime, GameRef.SpriteBatch, camera);

          
            
            if (isBehind)
            {
                hpManager.HidePlaceArray[hpIndex].Draw(gameTime, GameRef.SpriteBatch, camera, true);
            }
            //---Makha's code---//
            faceMan.Draw(gameTime, GameRef.SpriteBatch, camera);
            GameRef.SpriteBatch.DrawString(BritannicBold, "Level " + Level.ToString(), Vector2.Zero, Color.Blue);
            Vector2 scorePos = Vector2.Zero;
            scorePos.Y += 50;
            //drawing the number of checks
            GameRef.SpriteBatch.DrawString(BritannicBold, "# of Checks: " + hpManager.CurrentCheckCount.ToString(), scorePos, Color.Orange);
            //---End of Makha's code---//

            Vector2 handXY = new Vector2(InputHandler.hpos.X, InputHandler.hpos.Y);
            //GameRef.SpriteBatch.Draw(GameRef.Hand, handXY, Color.Blue);

            base.Draw(gameTime);

            GameRef.SpriteBatch.End();
        }

        #endregion

        #region Abstract Method Region
        #endregion
    }
}
