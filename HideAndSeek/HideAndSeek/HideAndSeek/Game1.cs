using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Kinect;

using XLibrary;
using XLibrary.GameScreens;

namespace XLibrary
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region XNA Field Region

        GraphicsDeviceManager graphics;
        public SpriteBatch SpriteBatch;
        SoundEffect soundEffect;
        InputHandler ih;
        Texture2D hand3;
        Texture2D hand2;
        Texture2D hand1;
        static int level;

        public int Level
        {
            set { level = value; }
            get { return level; }
        }

        public InputHandler IH
        {
            get { return ih; }
        }

        public Texture2D Hand3
        {
            get { return hand3; }
        }

        public Texture2D Hand2
        {
            get { return hand2; }
        }

        public Texture2D Hand1
        {
            get { return hand1; }
        }

        #endregion

        #region Game State Region

        GameStateManager stateManager;

     //   public TitleScreen TitleScreen;
        public StartMenuScreen StartMenuScreen;
        private GamePlayScreen GamePlayScreen;
        public StatsScreen StatsScreen;
        public AchievementsScreen AchievementsScreen;
        public OptionsScreen OptionsScreen;
        public FoundIt FoundIt;
        public PauseScreen PauseScreen;

        #endregion

        #region Screen Field Region

        const int screenWidth = 1024;
        const int screenHeight = 768;

        public readonly Rectangle ScreenRectangle;

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Level = 1;

            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            ScreenRectangle = new Rectangle(
                0,
                0,
                screenWidth,
                screenHeight);

            Content.RootDirectory = "Content";

            ih = new InputHandler(this);
            Components.Add(ih);

            stateManager = new GameStateManager(this);
            Components.Add(stateManager);


            if (ih.IsKinectPluggedIn())
            {
                IsMouseVisible = false;
            }
            else
            {
                IsMouseVisible = true;
            }

         //   TitleScreen = new TitleScreen(this, stateManager);
            StartMenuScreen = new StartMenuScreen(this, stateManager);
            GamePlayScreen = new GamePlayScreen(this, stateManager, 1);
            OptionsScreen = new OptionsScreen(this, stateManager);
            StatsScreen = new StatsScreen(this, stateManager);
            AchievementsScreen = new AchievementsScreen(this, stateManager);
            FoundIt = new FoundIt(this, stateManager);
            PauseScreen = new PauseScreen(this, stateManager);

            stateManager.ChangeState(StartMenuScreen);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                sensor.Stop();
            }
            base.OnExiting(sender, args);
        }

        public GamePlayScreen newGamePlayScreen()
        {
            GamePlayScreen = new GamePlayScreen(this,stateManager, 1);
            return GamePlayScreen;
        }

        public GamePlayScreen currentGamePlayScreen()
        {
            return GamePlayScreen;
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            //Shashank's code
            hand3 = Content.Load<Texture2D>("cursor3_128x128");
            hand2 = Content.Load<Texture2D>("cursor2_128x128");
            hand1 = Content.Load<Texture2D>("cursor1_128x128");
            soundEffect = Content.Load<SoundEffect>("click");
        }

        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }
    }
}
